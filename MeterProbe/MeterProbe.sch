EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MeterProbe
LIBS:MeterProbe-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Meter Probe for Optical Flag Protocol"
Date "25 oct 2015"
Rev "1.0"
Comp "Kirby & Co"
Comment1 "http://creativecommons.org/licenses/by-sa/3.0"
Comment2 "Licensing: CC Attribution-Share Alike 3.0 Unported"
Comment3 "Author: Andy Kirby (andy@kirbyand.co.uk, www.kirbyand.co.uk)"
Comment4 ""
$EndDescr
$Comp
L NPN Q1
U 1 1 562CED00
P 5500 3400
F 0 "Q1" H 5500 3250 50  0000 R CNN
F 1 "P2N2222" H 5500 3550 50  0000 R CNN
F 2 "~" H 5500 3400 60  0000 C CNN
F 3 "~" H 5500 3400 60  0000 C CNN
	1    5500 3400
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 562CED9C
P 4350 3050
F 0 "C1" H 4350 3150 40  0000 L CNN
F 1 "100nF" H 4356 2965 40  0000 L CNN
F 2 "~" H 4388 2900 30  0000 C CNN
F 3 "~" H 4350 3050 60  0000 C CNN
	1    4350 3050
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 562CEDC9
P 5600 2450
F 0 "R2" V 5680 2450 40  0000 C CNN
F 1 "2.7K" V 5607 2451 40  0000 C CNN
F 2 "~" V 5530 2450 30  0000 C CNN
F 3 "~" H 5600 2450 30  0000 C CNN
	1    5600 2450
	1    0    0    -1  
$EndComp
$Comp
L 74AHC1G14 U1
U 1 1 562CEDE9
P 6100 2850
F 0 "U1" H 6245 2965 40  0000 C CNN
F 1 "74AHC1G14" H 6300 2750 40  0000 C CNN
F 2 "~" H 6195 2715 30  0000 C CNN
F 3 "~" H 6245 2965 60  0000 C CNN
	1    6100 2850
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 562CEDF6
P 4850 2450
F 0 "R1" V 4930 2450 40  0000 C CNN
F 1 "51K" V 4857 2451 40  0000 C CNN
F 2 "~" V 4780 2450 30  0000 C CNN
F 3 "~" H 4850 2450 30  0000 C CNN
	1    4850 2450
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 562CEDFF
P 6750 3300
F 0 "R3" V 6830 3300 40  0000 C CNN
F 1 "180R" V 6757 3301 40  0000 C CNN
F 2 "~" V 6680 3300 30  0000 C CNN
F 3 "~" H 6750 3300 30  0000 C CNN
	1    6750 3300
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR1
U 1 1 562CEE3D
P 6050 2000
F 0 "#PWR1" H 6050 1960 30  0001 C CNN
F 1 "+3.3V" H 6050 2110 30  0000 C CNN
F 2 "" H 6050 2000 60  0000 C CNN
F 3 "" H 6050 2000 60  0000 C CNN
	1    6050 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR2
U 1 1 562CEE4C
P 6050 4300
F 0 "#PWR2" H 6050 4300 30  0001 C CNN
F 1 "GND" H 6050 4230 30  0001 C CNN
F 2 "" H 6050 4300 60  0000 C CNN
F 3 "" H 6050 4300 60  0000 C CNN
	1    6050 4300
	1    0    0    -1  
$EndComp
$Comp
L CONN_4 P1
U 1 1 562CEECA
P 7450 2900
F 0 "P1" V 7400 2900 50  0000 C CNN
F 1 "CONN_4" V 7500 2900 50  0000 C CNN
F 2 "" H 7450 2900 60  0000 C CNN
F 3 "" H 7450 2900 60  0000 C CNN
	1    7450 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2100 7000 2100
Wire Wire Line
	5600 2100 5600 2200
Wire Wire Line
	4850 2100 4850 2200
Connection ~ 5600 2100
Wire Wire Line
	4350 2100 4350 2850
Connection ~ 4850 2100
Wire Wire Line
	6750 4150 6750 4050
Wire Wire Line
	6750 3550 6750 3650
Wire Wire Line
	5600 4150 5600 3600
Connection ~ 6750 4150
Wire Wire Line
	4350 4150 4350 3250
Connection ~ 5600 4150
Wire Wire Line
	5600 2700 5600 3200
Wire Wire Line
	5650 2850 5600 2850
Connection ~ 5600 2850
Wire Wire Line
	6050 3050 6050 4300
Connection ~ 6050 4150
Wire Wire Line
	6050 2000 6050 2650
Connection ~ 6050 2100
Wire Wire Line
	6750 2950 6750 3050
Wire Wire Line
	6550 2850 7100 2850
Wire Wire Line
	6750 2950 7100 2950
Wire Wire Line
	7100 2750 7000 2750
Wire Wire Line
	7000 2750 7000 2100
Wire Wire Line
	7100 3050 7000 3050
Wire Wire Line
	7000 3050 7000 4150
Wire Wire Line
	7000 4150 4350 4150
$Comp
L IRLED D2
U 1 1 562CF5D3
P 6750 3850
F 0 "D2" H 6750 3950 50  0000 C CNN
F 1 "IRLED" H 6750 3700 50  0000 C CNN
F 2 "~" H 6750 3850 60  0000 C CNN
F 3 "~" H 6750 3850 60  0000 C CNN
	1    6750 3850
	0    1    1    0   
$EndComp
$Comp
L BPW34 D1
U 1 1 562CFBFA
P 4850 3050
F 0 "D1" H 4850 3150 50  0000 C CNN
F 1 "BPW34" H 4850 2900 50  0000 C CNN
F 2 "~" H 4850 3050 60  0000 C CNN
F 3 "~" H 4850 3050 60  0000 C CNN
	1    4850 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4850 2700 4850 2850
Wire Wire Line
	4850 3250 4850 3400
Wire Wire Line
	4850 3400 5300 3400
Text Notes 3300 2200 0    60   ~ 0
Meter LED:-\nOn, 12 uA\nOff, 0.1uA\nQ1 Gain aprox 100\n\nGiving:-\nON, 1.2mA\nOff, 0.1mA
$EndSCHEMATC
