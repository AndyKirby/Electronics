# A Minimalist Micro-Controller board for the ATmega 644p/1284P

![Hardware Photo](Min644P.jpg)

This is a design for a bare essentials micro-controller board for the ATMega 644p and 1284p both have the same 40 pin through hole footprint. Originaly inspired by the early versions of the [Sanguino](http://reprap.org/wiki/Burning_the_Sanguino_Bootloader_using_Arduino_as_ISP) this board was intended for a number of projects and being single sided with through hole components is intended to be easy to produce at home.   

I originally started using these as I wanted more io pins and peripherals. It was intended to run [Amforth](http://amforth.sourceforge.net/) but as well as being an Arduino based board it would be equally at home running [Bitlash](http://bitlash.net/) or [python on a chip](https://code.google.com/archive/p/python-on-a-chip/). As technology has progressed this design is still useful as a way to add essential peripherals and extra pins to NoT type wifi micro-controllers like the [ESP8266](https://github.com/esp8266/Arduino). 
