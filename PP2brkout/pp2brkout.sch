EESchema Schematic File Version 2
LIBS:power
LIBS:pp2brkout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Dual Parallel Port Breakout Board"
Date "30 jun 2009"
Rev "1"
Comp "Andy Kirby, Kirby & Co, andy@kirbyand.co.uk"
Comment1 "x"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 6350 4350
Connection ~ 6350 4550
Connection ~ 6350 4750
Connection ~ 6350 4950
Connection ~ 6350 5150
Connection ~ 6350 5350
Connection ~ 6350 5550
Wire Wire Line
	6350 5750 6350 3950
Wire Wire Line
	6350 5750 5700 5750
Wire Wire Line
	5700 1250 6350 1250
Wire Wire Line
	5700 1650 6350 1650
Wire Wire Line
	5700 2050 6350 2050
Wire Wire Line
	5700 2450 6350 2450
Wire Wire Line
	6350 5550 5700 5550
Wire Wire Line
	5700 5150 6350 5150
Wire Wire Line
	5700 4750 6350 4750
Wire Wire Line
	5700 4350 6350 4350
Wire Bus Line
	3100 5150 3100 6950
Wire Bus Line
	3100 6950 6100 6950
Wire Bus Line
	6100 6950 6100 4350
Wire Wire Line
	5700 5450 6000 5450
Wire Wire Line
	5700 5050 6000 5050
Wire Wire Line
	5700 4650 6000 4650
Wire Wire Line
	5700 4250 6000 4250
Wire Wire Line
	3000 6650 2700 6650
Wire Wire Line
	3000 6550 2700 6550
Wire Wire Line
	3000 6450 2700 6450
Wire Wire Line
	3000 6350 2700 6350
Wire Wire Line
	3000 6250 2700 6250
Wire Wire Line
	3000 6150 2700 6150
Wire Wire Line
	3000 6050 2700 6050
Wire Wire Line
	3000 5950 2700 5950
Wire Wire Line
	3000 5850 2700 5850
Wire Wire Line
	3000 5750 2700 5750
Wire Wire Line
	3000 5650 2700 5650
Wire Wire Line
	3000 5550 2700 5550
Wire Wire Line
	3000 5450 2700 5450
Wire Wire Line
	3000 5350 2700 5350
Wire Wire Line
	3000 5250 2700 5250
Wire Wire Line
	3000 5150 2700 5150
Wire Wire Line
	3000 5050 2700 5050
Wire Bus Line
	3100 2650 3100 850 
Wire Bus Line
	3100 850  6100 850 
Wire Bus Line
	6100 850  6100 3450
Wire Wire Line
	5700 1350 6000 1350
Wire Wire Line
	5700 1750 6000 1750
Wire Wire Line
	5700 2550 6000 2550
Wire Wire Line
	5700 2750 6000 2750
Wire Wire Line
	5700 2950 6000 2950
Wire Wire Line
	5700 3150 6000 3150
Wire Wire Line
	5700 3350 6000 3350
Wire Wire Line
	6000 3550 5700 3550
Wire Wire Line
	2700 1150 3000 1150
Wire Wire Line
	2650 3550 2750 3550
Connection ~ 2750 4050
Wire Wire Line
	2750 4050 2650 4050
Connection ~ 2750 3850
Connection ~ 2750 3650
Wire Wire Line
	2650 3650 2750 3650
Wire Wire Line
	2750 3550 2750 4250
Wire Wire Line
	2750 3750 2650 3750
Connection ~ 2750 3750
Connection ~ 2750 3950
Wire Wire Line
	2650 4150 2750 4150
Connection ~ 2750 4150
Wire Wire Line
	2750 4250 2650 4250
Wire Wire Line
	2700 1250 3000 1250
Wire Wire Line
	2700 1350 3000 1350
Wire Wire Line
	2700 1450 3000 1450
Wire Wire Line
	2700 1550 3000 1550
Wire Wire Line
	2700 1650 3000 1650
Wire Wire Line
	2700 1750 3000 1750
Wire Wire Line
	2700 1850 3000 1850
Wire Wire Line
	2700 1950 3000 1950
Wire Wire Line
	2700 2050 3000 2050
Wire Wire Line
	2700 2150 3000 2150
Wire Wire Line
	2700 2250 3000 2250
Wire Wire Line
	2700 2350 3000 2350
Wire Wire Line
	2700 2450 3000 2450
Wire Wire Line
	2700 2550 3000 2550
Wire Wire Line
	2700 2650 3000 2650
Wire Wire Line
	2700 2750 3000 2750
Wire Wire Line
	5700 3450 6000 3450
Wire Wire Line
	5700 3250 6000 3250
Wire Wire Line
	5700 3050 6000 3050
Wire Wire Line
	5700 2850 6000 2850
Wire Wire Line
	5700 2350 6000 2350
Wire Wire Line
	5700 2150 6000 2150
Wire Wire Line
	5700 1950 6000 1950
Wire Wire Line
	5700 1550 6000 1550
Wire Wire Line
	5700 1150 6000 1150
Wire Wire Line
	5700 5850 6000 5850
Wire Wire Line
	5700 5950 6000 5950
Wire Wire Line
	5700 6050 6000 6050
Wire Wire Line
	5700 6150 6000 6150
Wire Wire Line
	5700 6250 6000 6250
Wire Wire Line
	5700 6350 6000 6350
Wire Wire Line
	5700 6450 6000 6450
Wire Wire Line
	5700 6550 6000 6550
Wire Wire Line
	5700 6650 6000 6650
Wire Wire Line
	5700 4450 6000 4450
Wire Wire Line
	5700 4850 6000 4850
Wire Wire Line
	5700 5250 6000 5250
Wire Wire Line
	5700 5650 6000 5650
Wire Wire Line
	5700 4550 6350 4550
Wire Wire Line
	5700 4950 6350 4950
Wire Wire Line
	6350 5350 5700 5350
Wire Wire Line
	6350 3950 2650 3950
Wire Wire Line
	6350 2650 5700 2650
Wire Wire Line
	5700 2250 6350 2250
Wire Wire Line
	5700 1850 6350 1850
Wire Wire Line
	5700 1450 6350 1450
Wire Wire Line
	2650 3850 6350 3850
Wire Wire Line
	6350 3850 6350 1250
Connection ~ 6350 1450
Connection ~ 6350 1650
Connection ~ 6350 1850
Connection ~ 6350 2050
Connection ~ 6350 2250
Connection ~ 6350 2450
Connection ~ 6350 2650
Text Label 5750 4250 0    60   ~ 0
PB13
Text Label 5750 4450 0    60   ~ 0
PB12
Text Label 5750 4650 0    60   ~ 0
PB11
Text Label 5750 4850 0    60   ~ 0
PB10
Text Label 5750 5050 0    60   ~ 0
PB9
Text Label 5750 5250 0    60   ~ 0
PB8
Text Label 5750 5450 0    60   ~ 0
PB7
Text Label 5750 5650 0    60   ~ 0
PB6
Text Label 5750 5850 0    60   ~ 0
PB5
Text Label 5750 5950 0    60   ~ 0
PB17
Text Label 5750 6050 0    60   ~ 0
PB4
Text Label 5750 6150 0    60   ~ 0
PB16
Text Label 5750 6250 0    60   ~ 0
PB3
Text Label 5750 6350 0    60   ~ 0
PB15
Text Label 5750 6450 0    60   ~ 0
PB2
Text Label 5750 6550 0    60   ~ 0
PB14
Text Label 5750 6650 0    60   ~ 0
PB1
Text Notes 7600 7400 0    60   ~ 0
1
Text Notes 7500 7400 0    60   ~ 0
1
Entry Wire Line
	6000 6650 6100 6750
Entry Wire Line
	6000 6550 6100 6650
Entry Wire Line
	6000 6450 6100 6550
Entry Wire Line
	6000 6350 6100 6450
Entry Wire Line
	6000 6250 6100 6350
Entry Wire Line
	6000 6150 6100 6250
Entry Wire Line
	6000 6050 6100 6150
Entry Wire Line
	6000 5950 6100 6050
Entry Wire Line
	6000 5850 6100 5950
Entry Wire Line
	6000 5650 6100 5750
Entry Wire Line
	6000 5450 6100 5550
Entry Wire Line
	6000 5250 6100 5350
Entry Wire Line
	6000 5050 6100 5150
Entry Wire Line
	6000 4850 6100 4950
Entry Wire Line
	6000 4650 6100 4750
Entry Wire Line
	6000 4450 6100 4550
Entry Wire Line
	6000 4250 6100 4350
Entry Wire Line
	3000 6650 3100 6750
Entry Wire Line
	3000 6550 3100 6650
Entry Wire Line
	3000 6450 3100 6550
Entry Wire Line
	3000 6350 3100 6450
Entry Wire Line
	3000 6250 3100 6350
Entry Wire Line
	3000 6150 3100 6250
Entry Wire Line
	3000 6050 3100 6150
Entry Wire Line
	3000 5950 3100 6050
Entry Wire Line
	3000 5850 3100 5950
Entry Wire Line
	3000 5750 3100 5850
Entry Wire Line
	3000 5650 3100 5750
Entry Wire Line
	3000 5550 3100 5650
Entry Wire Line
	3000 5450 3100 5550
Entry Wire Line
	3000 5350 3100 5450
Entry Wire Line
	3000 5250 3100 5350
Entry Wire Line
	3000 5150 3100 5250
Entry Wire Line
	3000 5050 3100 5150
Text Label 2750 6650 0    60   ~ 0
PB17
Text Label 2750 6550 0    60   ~ 0
PB16
Text Label 2750 6450 0    60   ~ 0
PB15
Text Label 2750 6350 0    60   ~ 0
PB14
Text Label 2750 6250 0    60   ~ 0
PB13
Text Label 2750 6150 0    60   ~ 0
PB12
Text Label 2750 6050 0    60   ~ 0
PB11
Text Label 2750 5950 0    60   ~ 0
PB10
Text Label 2750 5850 0    60   ~ 0
PB9
Text Label 2750 5750 0    60   ~ 0
PB8
Text Label 2750 5650 0    60   ~ 0
PB7
Text Label 2750 5550 0    60   ~ 0
PB6
Text Label 2750 5450 0    60   ~ 0
PB5
Text Label 2750 5350 0    60   ~ 0
PB4
Text Label 2750 5250 0    60   ~ 0
PB3
Text Label 2750 5150 0    60   ~ 0
PB2
Text Label 2750 5050 0    60   ~ 0
PB1
Entry Wire Line
	6000 3550 6100 3450
Entry Wire Line
	6000 3450 6100 3350
Entry Wire Line
	6000 3350 6100 3250
Entry Wire Line
	6000 3250 6100 3150
Entry Wire Line
	6000 3150 6100 3050
Entry Wire Line
	6000 3050 6100 2950
Entry Wire Line
	6000 2950 6100 2850
Entry Wire Line
	6000 2850 6100 2750
Entry Wire Line
	6000 2750 6100 2650
Entry Wire Line
	6000 2550 6100 2450
Entry Wire Line
	6000 2350 6100 2250
Entry Wire Line
	6000 2150 6100 2050
Entry Wire Line
	6000 1950 6100 1850
Entry Wire Line
	6000 1750 6100 1650
Entry Wire Line
	6000 1550 6100 1450
Entry Wire Line
	6000 1350 6100 1250
Entry Wire Line
	6000 1150 6100 1050
Text Label 5750 3550 0    60   ~ 0
PA1
Text Label 5750 3450 0    60   ~ 0
PA14
Text Label 5750 3350 0    60   ~ 0
PA2
Text Label 5750 3250 0    60   ~ 0
PA15
Text Label 5750 3150 0    60   ~ 0
PA3
Text Label 5750 3050 0    60   ~ 0
PA16
Text Label 5750 2950 0    60   ~ 0
PA4
Text Label 5750 2850 0    60   ~ 0
PA17
Text Label 5750 2750 0    60   ~ 0
PA5
Text Label 5750 2550 0    60   ~ 0
PA6
Text Label 5750 2350 0    60   ~ 0
PA7
Text Label 5750 2150 0    60   ~ 0
PA8
Text Label 5750 1950 0    60   ~ 0
PA9
Text Label 5750 1750 0    60   ~ 0
PA10
Text Label 5750 1550 0    60   ~ 0
PA11
Text Label 5750 1350 0    60   ~ 0
PA12
Text Label 5750 1150 0    60   ~ 0
PA13
Entry Wire Line
	3000 2750 3100 2650
Entry Wire Line
	3000 2650 3100 2550
Entry Wire Line
	3000 2550 3100 2450
Entry Wire Line
	3000 2450 3100 2350
Entry Wire Line
	3000 2350 3100 2250
Entry Wire Line
	3000 2250 3100 2150
Entry Wire Line
	3000 2150 3100 2050
Entry Wire Line
	3000 2050 3100 1950
Entry Wire Line
	3000 1950 3100 1850
Entry Wire Line
	3000 1850 3100 1750
Entry Wire Line
	3000 1750 3100 1650
Entry Wire Line
	3000 1650 3100 1550
Entry Wire Line
	3000 1550 3100 1450
Entry Wire Line
	3000 1450 3100 1350
Entry Wire Line
	3000 1350 3100 1250
Entry Wire Line
	3000 1250 3100 1150
Entry Wire Line
	3000 1150 3100 1050
Text Label 2750 2750 0    60   ~ 0
PA17
Text Label 2750 2650 0    60   ~ 0
PA16
Text Label 2750 2550 0    60   ~ 0
PA15
Text Label 2750 2450 0    60   ~ 0
PA14
Text Label 2750 2350 0    60   ~ 0
PA13
Text Label 2750 2250 0    60   ~ 0
PA12
Text Label 2750 2150 0    60   ~ 0
PA11
Text Label 2750 2050 0    60   ~ 0
PA10
Text Label 2750 1950 0    60   ~ 0
PA9
Text Label 2750 1850 0    60   ~ 0
PA8
Text Label 2750 1750 0    60   ~ 0
PA7
Text Label 2750 1650 0    60   ~ 0
PA6
Text Label 2750 1550 0    60   ~ 0
PA5
Text Label 2750 1450 0    60   ~ 0
PA4
Text Label 2750 1350 0    60   ~ 0
PA3
Text Label 2750 1250 0    60   ~ 0
PA2
Text Label 2750 1150 0    60   ~ 0
PA1
$Comp
L CONN_8 P1
U 1 1 4A49CFC5
P 2300 3900
F 0 "P1" V 2250 3900 60  0000 C CNN
F 1 "CONN_8" V 2350 3900 60  0000 C CNN
F 2 "" H 2300 3900 60  0001 C CNN
F 3 "" H 2300 3900 60  0001 C CNN
	1    2300 3900
	-1   0    0    -1  
$EndComp
$Comp
L CONN_17 P2
U 1 1 4A49CFA6
P 2350 1950
F 0 "P2" V 2310 1950 60  0000 C CNN
F 1 "CONN_17" V 2430 1950 60  0000 C CNN
F 2 "" H 2350 1950 60  0001 C CNN
F 3 "" H 2350 1950 60  0001 C CNN
	1    2350 1950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_17 P3
U 1 1 4A49CF9C
P 2350 5850
F 0 "P3" V 2310 5850 60  0000 C CNN
F 1 "CONN_17" V 2430 5850 60  0000 C CNN
F 2 "" H 2350 5850 60  0001 C CNN
F 3 "" H 2350 5850 60  0001 C CNN
	1    2350 5850
	-1   0    0    -1  
$EndComp
$Comp
L DB25 J2
U 1 1 4A49CF1C
P 5250 5450
F 0 "J2" H 5300 6800 70  0000 C CNN
F 1 "DB25" H 5200 4100 70  0000 C CNN
F 2 "" H 5250 5450 60  0001 C CNN
F 3 "" H 5250 5450 60  0001 C CNN
	1    5250 5450
	-1   0    0    -1  
$EndComp
$Comp
L DB25 J1
U 1 1 4A49CEF5
P 5250 2350
F 0 "J1" H 5300 3700 70  0000 C CNN
F 1 "DB25" H 5200 1000 70  0000 C CNN
F 2 "" H 5250 2350 60  0001 C CNN
F 3 "" H 5250 2350 60  0001 C CNN
	1    5250 2350
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
