EESchema Schematic File Version 2  date Mon 28 Jan 2013 10:28:36 GMT
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:PktGC-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Pocket Geiger Counter based on EGM kit"
Date "27 jan 2013"
Rev "1.0"
Comp "Kirby & Co"
Comment1 "http://creativecommons.org/licenses/by-sa/3.0"
Comment2 "Licensing: CC Attribution-Share Alike 3.0 Unported"
Comment3 "Author: Andy Kirby (andy@kirbyand.co.uk, www.kirbyand.co.uk)"
Comment4 ""
$EndDescr
$Comp
L LM555N IC1
U 1 1 510633B9
P 3200 3500
F 0 "IC1" H 3200 3600 70  0000 C CNN
F 1 "LM555N" H 3200 3400 70  0000 C CNN
	1    3200 3500
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 510633D4
P 1900 2150
F 0 "#PWR?" H 1900 2250 30  0001 C CNN
F 1 "VCC" H 1900 2250 30  0000 C CNN
	1    1900 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 510633E8
P 1900 5150
F 0 "#PWR?" H 1900 5150 30  0001 C CNN
F 1 "GND" H 1900 5080 30  0001 C CNN
	1    1900 5150
	1    0    0    -1  
$EndComp
$Comp
L DARL_N Q3
U 1 1 5106344D
P 8000 4400
F 0 "Q3" H 8000 4650 50  0000 C CNN
F 1 "DARL_N" H 7800 4300 50  0000 C CNN
	1    8000 4400
	-1   0    0    -1  
$EndComp
$Comp
L PNP Q1
U 1 1 51063462
P 5500 4150
F 0 "Q1" H 5500 4000 60  0000 R CNN
F 1 "PNP" H 5500 4300 60  0000 R CNN
	1    5500 4150
	1    0    0    1   
$EndComp
$Comp
L TRANSFO T1
U 1 1 51063488
P 6100 4650
F 0 "T1" H 6100 4900 70  0000 C CNN
F 1 "TRANSFO" H 6100 4350 70  0000 C CNN
	1    6100 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4350 5600 4450
Wire Wire Line
	5600 4450 5700 4450
$Comp
L BATTERY BT1
U 1 1 510635A4
P 1900 4550
F 0 "BT1" H 1900 4750 50  0000 C CNN
F 1 "9V PP3 Battery" H 1900 4360 50  0000 C CNN
	1    1900 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	1900 4850 1900 5150
Wire Wire Line
	5600 4850 5600 5050
Wire Wire Line
	5600 4850 5700 4850
Wire Wire Line
	5600 3950 5600 2250
Wire Wire Line
	5600 2250 1900 2250
Wire Wire Line
	1900 5050 9250 5050
Connection ~ 1900 5050
Wire Wire Line
	6500 4850 6600 4850
Wire Wire Line
	6600 4850 6600 5050
Connection ~ 5600 5050
Connection ~ 6600 5050
$Comp
L C Cx
U 1 1 510636F2
P 7200 2250
F 0 "Cx" H 7250 2350 50  0000 L CNN
F 1 "10nF" H 7250 2150 50  0000 L CNN
	1    7200 2250
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 51063701
P 8750 4350
F 0 "R6" V 8830 4350 50  0000 C CNN
F 1 "10M" V 8750 4350 50  0000 C CNN
	1    8750 4350
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D?
U 1 1 51063710
P 6900 1950
F 0 "D?" H 6900 2050 40  0000 C CNN
F 1 "DIODE" H 6900 1850 40  0000 C CNN
	1    6900 1950
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5106372C
P 9250 4700
F 0 "R3" V 9330 4700 50  0000 C CNN
F 1 "100K" V 9250 4700 50  0000 C CNN
	1    9250 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 5050 9250 4950
Wire Wire Line
	9000 4350 9250 4350
Wire Wire Line
	9250 4250 9250 4450
Wire Wire Line
	8250 4350 8500 4350
Wire Wire Line
	7750 4550 7650 4550
Wire Wire Line
	7650 4550 7650 5050
Connection ~ 7650 5050
Wire Wire Line
	7100 1950 7300 1950
Wire Wire Line
	7200 1950 7200 2050
$Comp
L R R5
U 1 1 510637E3
P 7550 1950
F 0 "R5" V 7630 1950 50  0000 C CNN
F 1 "1M" V 7550 1950 50  0000 C CNN
	1    7550 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6500 4450 6600 4450
Wire Wire Line
	6600 4450 6600 1950
Wire Wire Line
	6600 1950 6700 1950
Wire Wire Line
	7200 2450 7200 5050
Connection ~ 7200 5050
Connection ~ 9250 4350
Wire Wire Line
	7800 1950 9250 1950
$Comp
L R Rx
U 1 1 51063A24
P 7650 3050
F 0 "Rx" V 7730 3050 50  0000 C CNN
F 1 "270R" V 7650 3050 50  0000 C CNN
	1    7650 3050
	1    0    0    -1  
$EndComp
$Comp
L LED L1
U 1 1 51063A2C
P 7650 3600
F 0 "L1" H 7650 3700 50  0000 C CNN
F 1 "LED" H 7650 3500 50  0000 C CNN
	1    7650 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	7650 3300 7650 3400
$Comp
L SPEAKER SPK
U 1 1 51063A88
P 8400 3250
F 0 "SPK" H 8300 3500 70  0000 C CNN
F 1 "SPEAKER" H 8300 3000 70  0000 C CNN
	1    8400 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 3150 8100 3150
Wire Wire Line
	8050 2700 8050 3150
Wire Wire Line
	8050 3900 8050 3350
Wire Wire Line
	8050 3350 8100 3350
$Comp
L VCC #PWR?
U 1 1 51063AEE
P 7850 2650
F 0 "#PWR?" H 7850 2750 30  0001 C CNN
F 1 "VCC" H 7850 2750 30  0000 C CNN
	1    7850 2650
	1    0    0    -1  
$EndComp
Connection ~ 1900 2250
Wire Wire Line
	7850 4150 7850 3900
Wire Wire Line
	7650 3900 8050 3900
Wire Wire Line
	7650 3900 7650 3800
Connection ~ 7850 3900
Wire Wire Line
	7650 2700 8050 2700
Connection ~ 7850 2700
Wire Wire Line
	7850 2700 7850 2650
Wire Wire Line
	7650 2700 7650 2800
Wire Notes Line
	9250 2450 9250 3800
Wire Notes Line
	9550 2600 9550 3950
Wire Notes Line
	9550 3950 8950 3950
Wire Notes Line
	8950 3950 8950 2600
Wire Notes Line
	9250 3950 9250 4100
$Comp
L CONN_1 P?
U 1 1 51063EEE
P 9250 2400
F 0 "P?" H 9330 2400 40  0000 L CNN
F 1 "CONN_1" H 9250 2455 30  0001 C CNN
	1    9250 2400
	0    1    1    0   
$EndComp
$Comp
L CONN_1 P?
U 1 1 51063EFD
P 9250 4100
F 0 "P?" H 9330 4100 40  0000 L CNN
F 1 "CONN_1" H 9250 4155 30  0001 C CNN
	1    9250 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9250 1950 9250 2250
$Comp
L R R1
U 1 1 5106430E
P 5150 3800
F 0 "R1" V 5230 3800 50  0000 C CNN
F 1 "4.7K" V 5150 3800 50  0000 C CNN
	1    5150 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4150 5300 4150
Wire Wire Line
	5150 4150 5150 4050
Wire Wire Line
	5150 3550 5150 2250
Connection ~ 5150 2250
$Comp
L CP1 C1
U 1 1 510643DA
P 4800 4150
F 0 "C1" H 4850 4250 50  0000 L CNN
F 1 "1uF" H 4850 4050 50  0000 L CNN
	1    4800 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 3500 4000 3500
Wire Wire Line
	4000 2850 4000 4050
Wire Wire Line
	4000 3700 3900 3700
$Comp
L C C2
U 1 1 5106445F
P 4000 4250
F 0 "C2" H 4050 4350 50  0000 L CNN
F 1 "0.47uF" H 4050 4150 50  0000 L CNN
	1    4000 4250
	1    0    0    -1  
$EndComp
Connection ~ 4000 3700
Wire Wire Line
	3900 3300 4500 3300
Wire Wire Line
	4500 3300 4500 4150
Wire Wire Line
	4500 4150 4600 4150
Wire Wire Line
	4000 4450 4000 5050
Connection ~ 4000 5050
NoConn ~ 2500 3550
$Comp
L R R2
U 1 1 51064562
P 4000 2600
F 0 "R2" V 4080 2600 50  0000 C CNN
F 1 "12K" V 4000 2600 50  0000 C CNN
	1    4000 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3300 2400 3300
Wire Wire Line
	2400 3300 2400 2950
Wire Wire Line
	2400 2950 4000 2950
Connection ~ 4000 3500
Wire Wire Line
	4000 2250 4000 2350
Connection ~ 4000 2250
$Comp
L SPST S1
U 1 1 51064896
P 1900 3650
F 0 "S1" H 1900 3750 70  0000 C CNN
F 1 "SPST" H 1900 3550 70  0000 C CNN
	1    1900 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1900 2150 1900 3150
Wire Wire Line
	1900 4150 1900 4250
Wire Wire Line
	1900 3000 2200 3000
Wire Wire Line
	2200 3000 2200 3800
Wire Wire Line
	2200 3800 2500 3800
Connection ~ 1900 3000
Text Notes 750  6800 0    60   ~ 0
Notes:-\n\n1. Components added to original EGM kit, Cx  and Rx\n2. Components Surplus after changes, C3, R4, Q2 and Large Capacitor\n3. Darlington base resistor R6 is unchanged but could probably be safely reduced to 100K\n4. Cx rated at 2KV\n5. HV is not regulated and is a function of step up transformer turns ratio,\n lower voltageGM tubes may be accomodated by using a lower voltage battery instead of PP3.\n6. Modified battery drain is aprox 8mA (reduced from 140mA) and tube voltage is aprox 850V.\n7. If running from a lower voltage battery the values of Rx & R6 may need adjusting to suit.
Text Notes 9650 3400 0    60   ~ 0
GM Tube
$EndSCHEMATC
