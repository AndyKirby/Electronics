EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:NoTLamp
LIBS:NoTLamp-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "NoTLamp Schematic"
Date "2016-03-07"
Rev "1.0"
Comp "Kirby & Co"
Comment1 "http://creativecommons.org/licenses/by-sa/3.0"
Comment2 "Licensing: CC Attribution-Share Alike 3.0 Unported"
Comment3 "Author: Andy Kirby (andy@kirbyand.co.uk, www.kirbyand.co.uk)"
Comment4 ""
$EndDescr
$Comp
L ESP8266-07 U?
U 1 1 56E1C653
P 4700 4850
F 0 "U?" H 4700 4850 60  0000 C CNN
F 1 "ESP8266-07" H 4700 3350 60  0000 C CNN
F 2 "" H 4700 4850 60  0000 C CNN
F 3 "" H 4700 4850 60  0000 C CNN
	1    4700 4850
	1    0    0    -1  
$EndComp
$Comp
L DHT22 U?
U 1 1 56E1C6BC
P 10650 1550
F 0 "U?" H 10650 1450 60  0000 C CNN
F 1 "DHT22" H 10650 1250 60  0000 C CNN
F 2 "" H 10650 1550 60  0000 C CNN
F 3 "" H 10650 1550 60  0000 C CNN
	1    10650 1550
	1    0    0    -1  
$EndComp
$Comp
L PIR U?
U 1 1 56E1C735
P 13250 8450
F 0 "U?" H 13250 8400 60  0000 C CNN
F 1 "PIR" H 13250 8200 60  0000 C CNN
F 2 "" H 13250 8450 60  0000 C CNN
F 3 "" H 13250 8450 60  0000 C CNN
	1    13250 8450
	0    -1   -1   0   
$EndComp
$Comp
L PIR U?
U 1 1 56E1C7D8
P 13250 7650
F 0 "U?" H 13250 7600 60  0000 C CNN
F 1 "PIR" H 13250 7400 60  0000 C CNN
F 2 "" H 13250 7650 60  0000 C CNN
F 3 "" H 13250 7650 60  0000 C CNN
	1    13250 7650
	0    -1   -1   0   
$EndComp
$Comp
L PIR U?
U 1 1 56E1C859
P 13250 6850
F 0 "U?" H 13250 6800 60  0000 C CNN
F 1 "PIR" H 13250 6600 60  0000 C CNN
F 2 "" H 13250 6850 60  0000 C CNN
F 3 "" H 13250 6850 60  0000 C CNN
	1    13250 6850
	0    -1   -1   0   
$EndComp
$Comp
L PIR U?
U 1 1 56E1C8E0
P 13250 6050
F 0 "U?" H 13250 6000 60  0000 C CNN
F 1 "PIR" H 13250 5800 60  0000 C CNN
F 2 "" H 13250 6050 60  0000 C CNN
F 3 "" H 13250 6050 60  0000 C CNN
	1    13250 6050
	0    -1   -1   0   
$EndComp
$Comp
L PIR U?
U 1 1 56E1C94F
P 13250 5250
F 0 "U?" H 13250 5200 60  0000 C CNN
F 1 "PIR" H 13250 5000 60  0000 C CNN
F 2 "" H 13250 5250 60  0000 C CNN
F 3 "" H 13250 5250 60  0000 C CNN
	1    13250 5250
	0    -1   -1   0   
$EndComp
$Comp
L Photores R?
U 1 1 56E1D031
P 6800 2350
F 0 "R?" V 6880 2350 50  0000 C CNN
F 1 "Photores" V 7010 2350 50  0000 C TNN
F 2 "" V 6730 2350 50  0000 C CNN
F 3 "" H 6800 2350 50  0000 C CNN
	1    6800 2350
	1    0    0    -1  
$EndComp
$Comp
L SK6812_LED_Strip U?
U 1 1 56E1D3D4
P 14000 1750
F 0 "U?" H 14000 1650 60  0000 C CNN
F 1 "SK6812_LED_Strip" H 14000 1750 60  0000 C CNN
F 2 "" H 14000 1750 60  0000 C CNN
F 3 "" H 14000 1750 60  0000 C CNN
	1    14000 1750
	1    0    0    -1  
$EndComp
Text Notes 1000 8950 0    60   ~ 0
GPIO15
Text Notes 1550 8950 0    60   ~ 0
GPIO0
Text Notes 2200 8950 0    60   ~ 0
GPIO2
Text Notes 2850 8950 0    60   ~ 0
Mode
Text Notes 3200 8950 0    60   ~ 0
Comments
Text Notes 1050 9200 0    60   ~ 0
Low
Text Notes 1050 9300 0    60   ~ 0
Low
Text Notes 1050 9400 0    60   ~ 0
High
Text Notes 1600 9200 0    60   ~ 0
High
Text Notes 1600 9300 0    60   ~ 0
Low
Text Notes 1600 9400 0    60   ~ 0
Don't Care
Text Notes 2250 9200 0    60   ~ 0
High
Text Notes 2250 9300 0    60   ~ 0
High
Text Notes 2250 9400 0    60   ~ 0
Don't Care
Text Notes 2850 9200 0    60   ~ 0
Flash
Text Notes 2850 9300 0    60   ~ 0
UART
Text Notes 2850 9400 0    60   ~ 0
SDIO
Text Notes 3200 9200 0    60   ~ 0
Boot from SPI Flash
Text Notes 3200 9300 0    60   ~ 0
Program via UART
Text Notes 3200 9400 0    60   ~ 0
Boot from SD Card
Wire Notes Line
	1450 8850 1450 9450
Wire Notes Line
	2150 8850 2150 9450
Wire Notes Line
	2800 8850 2800 9450
Wire Notes Line
	3150 8850 3150 9450
Wire Notes Line
	950  9050 4250 9050
Wire Notes Line
	950  9450 4250 9450
Wire Notes Line
	950  8800 4250 8800
Wire Notes Line
	4250 8800 4250 9450
Wire Notes Line
	950  8800 950  9450
$Comp
L +3V3 #PWR?
U 1 1 56E200DE
P 10250 3000
F 0 "#PWR?" H 10250 2850 50  0001 C CNN
F 1 "+3V3" H 10250 3140 50  0000 C CNN
F 2 "" H 10250 3000 50  0000 C CNN
F 3 "" H 10250 3000 50  0000 C CNN
	1    10250 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 56E2010D
P 9650 8400
F 0 "#PWR?" H 9650 8150 50  0001 C CNN
F 1 "GND" H 9650 8250 50  0000 C CNN
F 2 "" H 9650 8400 50  0000 C CNN
F 3 "" H 9650 8400 50  0000 C CNN
	1    9650 8400
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 56E2017D
P 9350 7800
F 0 "R?" V 9430 7800 50  0000 C CNN
F 1 "10K" V 9350 7800 50  0000 C CNN
F 2 "" V 9280 7800 50  0000 C CNN
F 3 "" H 9350 7800 50  0000 C CNN
	1    9350 7800
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 56E201C6
P 9050 7800
F 0 "R?" V 9130 7800 50  0000 C CNN
F 1 "10K" V 9050 7800 50  0000 C CNN
F 2 "" V 8980 7800 50  0000 C CNN
F 3 "" H 9050 7800 50  0000 C CNN
	1    9050 7800
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 56E20220
P 9650 7800
F 0 "R?" V 9730 7800 50  0000 C CNN
F 1 "10K" V 9650 7800 50  0000 C CNN
F 2 "" V 9580 7800 50  0000 C CNN
F 3 "" H 9650 7800 50  0000 C CNN
	1    9650 7800
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR?
U 1 1 56E20397
P 9050 7100
F 0 "#PWR?" H 9050 6950 50  0001 C CNN
F 1 "+3V3" H 9050 7240 50  0000 C CNN
F 2 "" H 9050 7100 50  0000 C CNN
F 3 "" H 9050 7100 50  0000 C CNN
	1    9050 7100
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 56E2039D
P 8750 7800
F 0 "R?" V 8830 7800 50  0000 C CNN
F 1 "1K" V 8750 7800 50  0000 C CNN
F 2 "" V 8680 7800 50  0000 C CNN
F 3 "" H 8750 7800 50  0000 C CNN
	1    8750 7800
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 56E203A3
P 6800 2850
F 0 "R?" V 6880 2850 50  0000 C CNN
F 1 "10K" V 6800 2850 50  0000 C CNN
F 2 "" V 6730 2850 50  0000 C CNN
F 3 "" H 6800 2850 50  0000 C CNN
	1    6800 2850
	1    0    0    -1  
$EndComp
Text Notes 950  9700 0    60   ~ 0
Notes:-\n
Text Notes 950  10000 0    60   ~ 0
CH_PD Chip Power Down, drive low to put ESP in power down mode even halts timers etc. Hold high to ensure smooth running.
Text Notes 950  10150 0    60   ~ 0
Reset, drive low to reset the ESP. Hold high to ensure smooth running
Text Notes 950  10300 0    60   ~ 0
GPIO15,0,2, Have implicaitons for bootup ensure pins default to sensible values at power on. Once booted can be used for other things.
$Comp
L GND #PWR?
U 1 1 56E29DFF
P 9100 5200
F 0 "#PWR?" H 9100 4950 50  0001 C CNN
F 1 "GND" H 9100 5050 50  0000 C CNN
F 2 "" H 9100 5200 50  0000 C CNN
F 3 "" H 9100 5200 50  0000 C CNN
	1    9100 5200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 56E29E6A
P 10900 3300
F 0 "#PWR?" H 10900 3050 50  0001 C CNN
F 1 "GND" H 10900 3150 50  0000 C CNN
F 2 "" H 10900 3300 50  0000 C CNN
F 3 "" H 10900 3300 50  0000 C CNN
	1    10900 3300
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 P?
U 1 1 56E2A03D
P 9350 4950
F 0 "P?" H 9350 5250 50  0000 C CNN
F 1 "CONN_01X05" V 9450 4950 50  0000 C CNN
F 2 "" H 9350 4950 50  0000 C CNN
F 3 "" H 9350 4950 50  0000 C CNN
	1    9350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 5150 9100 5150
Wire Wire Line
	9100 5150 9100 5200
Wire Wire Line
	9150 5050 8700 5050
Wire Wire Line
	9150 4950 8700 4950
Wire Wire Line
	9150 4850 8700 4850
Wire Wire Line
	9150 4750 8700 4750
Text Label 8800 4750 0    60   ~ 0
TXD
Text Label 8800 4850 0    60   ~ 0
RXD
Text Label 8800 4950 0    60   ~ 0
GPIO0
Text Label 8800 5050 0    60   ~ 0
Reset
Text Notes 8750 4550 0    60   ~ 0
Load Firmware
Wire Wire Line
	5250 5100 5700 5100
Wire Wire Line
	5250 5250 5700 5250
Wire Wire Line
	5250 5700 5700 5700
Wire Wire Line
	4150 5100 3700 5100
Text Label 3750 5100 0    60   ~ 0
Reset
Text Label 5300 5100 0    60   ~ 0
TXD
Text Label 5300 5250 0    60   ~ 0
RXD
Text Label 5300 5700 0    60   ~ 0
GPIO0
Text Notes 950  9850 0    60   ~ 0
Design is 3v3 throughout, no level shifitng or ability to work with 5V has been added.
Wire Wire Line
	5250 6000 5700 6000
Text Label 5300 6000 0    60   ~ 0
GPIO15
Wire Wire Line
	8750 7150 8750 7650
Wire Wire Line
	8750 7150 9350 7150
Wire Wire Line
	9050 7100 9050 7650
Connection ~ 9050 7150
Wire Wire Line
	9350 7150 9350 7650
Wire Wire Line
	9350 7950 9350 8400
Wire Wire Line
	9050 7950 9050 8400
Wire Wire Line
	8750 7950 8750 8400
Wire Wire Line
	9650 7650 9650 7200
Wire Wire Line
	9650 7950 9650 8400
Text Label 8750 8000 3    60   ~ 0
GPIO0
Text Label 9050 8000 3    60   ~ 0
Reset
Text Label 9350 8000 3    60   ~ 0
CH_PD
Text Label 9650 7300 3    60   ~ 0
GPIO15
Wire Wire Line
	14200 5400 14250 5400
Wire Wire Line
	14250 5400 14250 8600
Wire Wire Line
	14250 6200 14200 6200
Wire Wire Line
	14250 7000 14200 7000
Connection ~ 14250 6200
Wire Wire Line
	14250 7800 14200 7800
Connection ~ 14250 7000
Wire Wire Line
	14250 8600 14200 8600
Connection ~ 14250 7800
Wire Wire Line
	14200 5100 14350 5100
Wire Wire Line
	14350 5100 14350 8300
Wire Wire Line
	14350 5900 14200 5900
Wire Wire Line
	14350 6700 14200 6700
Connection ~ 14350 5900
Wire Wire Line
	14350 7500 14200 7500
Connection ~ 14350 6700
Wire Wire Line
	14350 8300 14200 8300
Connection ~ 14350 7500
Wire Wire Line
	14200 5250 14800 5250
Wire Wire Line
	14200 6050 14800 6050
Wire Wire Line
	14200 6850 14800 6850
Wire Wire Line
	14200 7650 14800 7650
Wire Wire Line
	14200 8450 14800 8450
Text Notes 950  10450 0    60   ~ 0
Dev LED, for development and debuging cut link and add LED
Text Label 14450 6050 0    60   ~ 0
GPIO4
Text Label 14450 5250 0    60   ~ 0
GPIO2
Text Label 14450 6850 0    60   ~ 0
GPIO5
Text Label 14450 7650 0    60   ~ 0
GPIO12
Text Label 14450 8450 0    60   ~ 0
GPIO13
Wire Wire Line
	5250 5400 5700 5400
Wire Wire Line
	5250 5550 5700 5550
Wire Wire Line
	5250 5850 5700 5850
Wire Wire Line
	4150 5850 3700 5850
Wire Wire Line
	4150 6000 3700 6000
Text Label 5300 5400 0    60   ~ 0
GPIO4
Text Label 5300 5550 0    60   ~ 0
GPIO5
Text Label 5300 5850 0    60   ~ 0
GPIO2
Text Label 3750 5850 0    60   ~ 0
GPIO12
Text Label 3750 6000 0    60   ~ 0
GPIO13
$Comp
L C C?
U 1 1 56E3E2C9
P 10700 3200
F 0 "C?" H 10725 3300 50  0000 L CNN
F 1 "C" H 10725 3100 50  0000 L CNN
F 2 "" H 10738 3050 50  0000 C CNN
F 3 "" H 10700 3200 50  0000 C CNN
	1    10700 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10900 2500 10900 3300
Wire Wire Line
	10900 2550 10750 2550
Wire Wire Line
	10750 2550 10750 2500
Wire Wire Line
	10600 2500 10600 2950
Wire Wire Line
	10900 3200 10850 3200
Connection ~ 10900 2550
Connection ~ 10900 3200
Wire Wire Line
	10450 2500 10450 3200
Wire Wire Line
	10250 3200 10550 3200
Wire Wire Line
	10250 3000 10250 3200
Connection ~ 10450 3200
Text Label 10600 2600 3    60   ~ 0
GPIO0
Text Notes 10100 1550 0    60   ~ 0
Temperature & Humidity\nSensor
Wire Wire Line
	4150 5250 3700 5250
Text Label 3750 5250 0    60   ~ 0
ADC
$Comp
L +3V3 #PWR?
U 1 1 56E3E93F
P 3500 6100
F 0 "#PWR?" H 3500 5950 50  0001 C CNN
F 1 "+3V3" H 3500 6240 50  0000 C CNN
F 2 "" H 3500 6100 50  0000 C CNN
F 3 "" H 3500 6100 50  0000 C CNN
	1    3500 6100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 56E3E97A
P 5900 6200
F 0 "#PWR?" H 5900 5950 50  0001 C CNN
F 1 "GND" H 5900 6050 50  0000 C CNN
F 2 "" H 5900 6200 50  0000 C CNN
F 3 "" H 5900 6200 50  0000 C CNN
	1    5900 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6100 3500 6150
Wire Wire Line
	3500 6150 4150 6150
Wire Wire Line
	5250 6150 5900 6150
Wire Wire Line
	5900 6150 5900 6200
Wire Wire Line
	4150 5400 3700 5400
Wire Wire Line
	4150 5550 3700 5550
Wire Wire Line
	4150 5700 3700 5700
Text Label 3750 5700 0    60   ~ 0
GPIO14
Text Label 3750 5550 0    60   ~ 0
GPIO16
Text Label 3750 5400 0    60   ~ 0
CH_PD
$Comp
L GND #PWR?
U 1 1 56E437CB
P 14200 3200
F 0 "#PWR?" H 14200 2950 50  0001 C CNN
F 1 "GND" H 14200 3050 50  0000 C CNN
F 2 "" H 14200 3200 50  0000 C CNN
F 3 "" H 14200 3200 50  0000 C CNN
	1    14200 3200
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR?
U 1 1 56E43946
P 13450 2650
F 0 "#PWR?" H 13450 2500 50  0001 C CNN
F 1 "+3V3" H 13450 2790 50  0000 C CNN
F 2 "" H 13450 2650 50  0000 C CNN
F 3 "" H 13450 2650 50  0000 C CNN
	1    13450 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	14050 2550 14050 3000
Text Label 14050 2650 3    60   ~ 0
GPIO15
Wire Wire Line
	13900 2750 13900 2550
Wire Wire Line
	13350 2750 13900 2750
Wire Wire Line
	13450 2750 13450 2650
Wire Wire Line
	13750 2550 13750 3100
Wire Wire Line
	13700 3100 14200 3100
Wire Wire Line
	14200 2550 14200 3200
Connection ~ 14200 3100
Text Notes 13700 1550 0    60   ~ 0
Lamp LED's
$Comp
L C C?
U 1 1 56E454BE
P 13550 3100
F 0 "C?" H 13575 3200 50  0000 L CNN
F 1 "C" H 13575 3000 50  0000 L CNN
F 2 "" H 13588 2950 50  0000 C CNN
F 3 "" H 13550 3100 50  0000 C CNN
	1    13550 3100
	0    -1   -1   0   
$EndComp
Connection ~ 13750 3100
Wire Wire Line
	13400 3100 13350 3100
Wire Wire Line
	13350 3100 13350 2750
Connection ~ 13450 2750
$Comp
L C C?
U 1 1 56E45922
P 4700 6600
F 0 "C?" H 4725 6700 50  0000 L CNN
F 1 "C" H 4725 6500 50  0000 L CNN
F 2 "" H 4738 6450 50  0000 C CNN
F 3 "" H 4700 6600 50  0000 C CNN
	1    4700 6600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4850 6600 5300 6600
Wire Wire Line
	5300 6600 5300 6150
Connection ~ 5300 6150
Wire Wire Line
	4550 6600 4100 6600
Wire Wire Line
	4100 6600 4100 6150
Connection ~ 4100 6150
Text Notes 4300 4600 0    60   ~ 0
Wifi Microcontroler
Text Notes 13250 4750 0    60   ~ 0
360 Degree PIR Motion Sensors
Text Notes 6900 1550 0    60   ~ 0
Light Level Sensor
Text Notes 2350 1550 0    60   ~ 0
Sound Level Sensor
$Comp
L GND #PWR?
U 1 1 56E477D4
P 6800 3100
F 0 "#PWR?" H 6800 2850 50  0001 C CNN
F 1 "GND" H 6800 2950 50  0000 C CNN
F 2 "" H 6800 3100 50  0000 C CNN
F 3 "" H 6800 3100 50  0000 C CNN
	1    6800 3100
	1    0    0    -1  
$EndComp
$Comp
L D D?
U 1 1 56E47817
P 7200 2650
F 0 "D?" H 7200 2750 50  0000 C CNN
F 1 "1N4148" H 7200 2550 50  0000 C CNN
F 2 "" H 7200 2650 50  0000 C CNN
F 3 "" H 7200 2650 50  0000 C CNN
	1    7200 2650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6800 2600 6800 2700
Wire Wire Line
	6800 2650 7050 2650
Connection ~ 6800 2650
Wire Wire Line
	6800 3000 6800 3100
Wire Wire Line
	7350 2650 7800 2650
Wire Wire Line
	6800 2100 6800 1650
Text Label 6800 2050 1    60   ~ 0
GPIO14
Text Label 7500 2650 0    60   ~ 0
ADC
Wire Wire Line
	2700 1700 2700 2150
Text Label 2700 1750 3    60   ~ 0
GPIO16
$Comp
L MIC_ELECTRET U?
U 1 1 56E2C860
P 2350 2750
F 0 "U?" H 2350 3000 60  0000 C CNN
F 1 "MIC_ELECTRET" H 2000 2750 60  0000 C CNN
F 2 "" H 2350 2750 60  0000 C CNN
F 3 "" H 2350 2750 60  0000 C CNN
	1    2350 2750
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 56E2CA01
P 2950 2550
F 0 "C?" H 2975 2650 50  0000 L CNN
F 1 "C" H 2975 2450 50  0000 L CNN
F 2 "" H 2988 2400 50  0000 C CNN
F 3 "" H 2950 2550 50  0000 C CNN
	1    2950 2550
	0    -1   -1   0   
$EndComp
$Comp
L R R?
U 1 1 56E2CAC9
P 2700 2300
F 0 "R?" V 2780 2300 50  0000 C CNN
F 1 "10K" V 2700 2300 50  0000 C CNN
F 2 "" V 2630 2300 50  0000 C CNN
F 3 "" H 2700 2300 50  0000 C CNN
	1    2700 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2450 2700 2650
Wire Wire Line
	2700 2550 2800 2550
Wire Wire Line
	2700 2650 2650 2650
Connection ~ 2700 2550
$Comp
L GND #PWR?
U 1 1 56E2CE36
P 3450 3050
F 0 "#PWR?" H 3450 2800 50  0001 C CNN
F 1 "GND" H 3450 2900 50  0000 C CNN
F 2 "" H 3450 3050 50  0000 C CNN
F 3 "" H 3450 3050 50  0000 C CNN
	1    3450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 2850 2700 2850
Wire Wire Line
	2700 2850 2700 3000
$Comp
L C C?
U 1 1 56E2CFE0
P 3150 2800
F 0 "C?" H 3175 2900 50  0000 L CNN
F 1 "C" H 3175 2700 50  0000 L CNN
F 2 "" H 3188 2650 50  0000 C CNN
F 3 "" H 3150 2800 50  0000 C CNN
	1    3150 2800
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 56E2D09A
P 3450 2800
F 0 "R?" V 3530 2800 50  0000 C CNN
F 1 "10K" V 3450 2800 50  0000 C CNN
F 2 "" V 3380 2800 50  0000 C CNN
F 3 "" H 3450 2800 50  0000 C CNN
	1    3450 2800
	1    0    0    -1  
$EndComp
$Comp
L D D?
U 1 1 56E2D124
P 3800 2550
F 0 "D?" H 3800 2650 50  0000 C CNN
F 1 "1N4148" H 3800 2450 50  0000 C CNN
F 2 "" H 3800 2550 50  0000 C CNN
F 3 "" H 3800 2550 50  0000 C CNN
	1    3800 2550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 2550 3650 2550
Wire Wire Line
	3150 2650 3150 2550
Connection ~ 3150 2550
Wire Wire Line
	3450 2650 3450 2550
Connection ~ 3450 2550
Wire Wire Line
	3450 2950 3450 3050
Wire Wire Line
	2700 3000 3450 3000
Wire Wire Line
	3150 2950 3150 3000
Connection ~ 3150 3000
Connection ~ 3450 3000
Wire Wire Line
	3950 2550 4400 2550
Text Label 4150 2550 0    60   ~ 0
ADC
Text Notes 950  10600 0    60   ~ 0
DHT22/AM2302, drive low to signal start condition
Text Notes 950  10750 0    60   ~ 0
SK6812/WS2812, drive high to signal start condition
Text Notes 8850 6700 0    60   ~ 0
Pull Up's/Down's
$EndSCHEMATC
