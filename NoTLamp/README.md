A WiFi sensor cluster for a home automation NoT (Network of things).

The sensor cluster is built as a lamp and functions as an automatic lamp. It is mains powered and monitors the home environment, reporting it's state via WiFi and MQTT (Mosquitto). Using MQTT as a middle layer seperates out the NoT from the command, control and overview.

This is the electronics for the ESP8266 part of the lamp.

The electronics are a work in progress and start simple as a reporting automatic lamp that also monitors light level, room occupation, temperature and humidity. Thereafter features will be added on an adhoc basis until there are no longer any resources available (Amps, Pins, RAM, Processor Cycles)

Wish List of extras:-

* Automatic soft lighting during night time operation
* Flickering colour light to simulate a TV within the room
* Electronics, https://github.com/AndyKirby/Electronics/tree/master/NoTLamp
* Carbon dioxide measurement

Use of a MQTT broker for reporting and remote control allows the design of this sensor cluster to be controler agnostic. Making it suitabel for use with custom wroten controlers, web based controlers and other FOSS controlers ie Node Red. 

The electronics are for use with:-

* Printables, https://github.com/AndyKirby/PrintableParts/tree/master/NoTLamp 
* Firmware, https://github.com/AndyKirby/Firmware/tree/master/NoTLamp 

The design was realised using FOSS throughout. Electronic schematic capture and if we need it PCB's were designed using Kicad. Additional component and footprint libraries have been created or open sourced as needed. Any libraries created for this project can be found in this repository alongside the designs that use them.

* Debian Linux, https://www.debian.org/distrib/
* Kicad, http://kicad-pcb.org/
 
