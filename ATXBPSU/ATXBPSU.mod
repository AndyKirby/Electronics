PCBNEW-LibModule-V1  Sun 17 May 2015 14:04:06 BST
# encoding utf-8
Units mm
$INDEX
ATXPCBPlug
M4BPost
SPDT
$EndINDEX
$MODULE ATXPCBPlug
Po 0 0 0 15 55576A16 00000000 ~~
Li ATXPCBPlug
Sc 0
AR 
Op 0 0 0
T0 7.8 0 1 1 0 0.15 N V 21 N "ATXPCBPlug"
T1 4.6 46.4 1 1 0 0.15 N V 21 N "VAL**"
DS 14 19.6 11.4 19.6 0.15 21
DS 11.4 19.6 11.4 26.6 0.15 21
DS 11.4 26.6 14 26.6 0.15 21
DS 14 -2 13.8 -2 0.15 21
DS 14 48.2 14 -2 0.15 21
DS 2 48.2 14 48.2 0.15 21
DS 13.8 -2 2 -2 0.15 21
DS -7.4 -2 2 -2 0.15 21
DS 2 -2 2 48.2 0.15 21
DS 2 48.2 -7.4 48.2 0.15 21
DS -7.4 48.2 -7.4 -2 0.15 21
$PAD
Sh "13" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 0
$EndPAD
$PAD
Sh "14" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 4.2
$EndPAD
$PAD
Sh "15" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 8.4
$EndPAD
$PAD
Sh "16" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 12.6
$EndPAD
$PAD
Sh "17" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 16.8
$EndPAD
$PAD
Sh "18" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 21
$EndPAD
$PAD
Sh "19" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 25.2
$EndPAD
$PAD
Sh "20" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 29.4
$EndPAD
$PAD
Sh "21" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 33.6
$EndPAD
$PAD
Sh "22" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 37.8
$EndPAD
$PAD
Sh "23" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 42
$EndPAD
$PAD
Sh "24" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5.4 46.2
$EndPAD
$PAD
Sh "1" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 4.2
$EndPAD
$PAD
Sh "3" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 8.4
$EndPAD
$PAD
Sh "4" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 12.6
$EndPAD
$PAD
Sh "5" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 16.8
$EndPAD
$PAD
Sh "6" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 21
$EndPAD
$PAD
Sh "7" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 25.2
$EndPAD
$PAD
Sh "8" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 29.4
$EndPAD
$PAD
Sh "9" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 33.6
$EndPAD
$PAD
Sh "10" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 37.8
$EndPAD
$PAD
Sh "11" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 42
$EndPAD
$PAD
Sh "12" C 2.8 2.8 0 0 0
Dr 1.4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 46.2
$EndPAD
$EndMODULE ATXPCBPlug
$MODULE M4BPost
Po 0 0 0 15 55571279 00000000 ~~
Li M4BPost
Kw DEV
Sc 0
AR 1pin
Op 0 0 0
T0 0 -7.62 1.016 1.016 0 0.254 N V 21 N "M4BPost"
T1 0 7.62 1.016 1.016 0 0.254 N I 21 N "P***"
DC 0 0 5.08 2.54 1 21
$PAD
Sh "1" C 9 9 0 0 0
Dr 4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE M4BPost
$MODULE SPDT
Po 0.32 0.01 0 15 555891B8 00000000 ~~
Li SPDT
Sc 0
AR /55577E6C
Op 0 0 0
T0 0 -8.89 1 1 0 0.15 N V 21 N "SW1"
T1 0 8.89 1 1 0 0.15 N V 21 N "SWITCH_INV"
DS -5.08 -7.62 -5.08 -3.81 0.15 21
DS -5.08 -3.81 -8.89 -3.81 0.15 21
DS -8.89 -3.81 -8.89 3.81 0.15 21
DS -8.89 3.81 -5.08 3.81 0.15 21
DS -5.08 3.81 -5.08 7.62 0.15 21
DS -5.08 7.62 5.08 7.62 0.15 21
DS 5.08 7.62 5.08 3.81 0.15 21
DS 5.08 3.81 8.89 3.81 0.15 21
DS 8.89 3.81 8.89 -3.81 0.15 21
DS 8.89 -3.81 5.08 -3.81 0.15 21
DS 5.08 -3.81 5.08 -7.62 0.15 21
DS 5.08 -7.62 -5.08 -7.62 0.15 21
$PAD
Sh "1" C 1.3 1.3 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -7.62 -2.54
$EndPAD
$PAD
Sh "3" C 1.3 1.3 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 1 "GND"
Po -7.62 2.54
$EndPAD
$PAD
Sh "2" C 1.3 1.3 0 0 0
Dr 0.8 0 0
At STD N 00E0FFFF
Ne 2 "N-0000013"
Po 7.62 0
$EndPAD
$PAD
Sh "" C 12 12 0 0 0
Dr 6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "" C 2.2 2.2 0 0 0
Dr 2 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 6.35
$EndPAD
$EndMODULE SPDT
$EndLIBRARY
