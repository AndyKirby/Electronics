# ATXBPSU

A project to make a flying breakout board for repuprosing PC ATX power supplys as multi rail bench power supplies.

A number of these have been built and donated to Sheffield Hardware Hackers and Makers.

http://www.sheffieldhardwarehackers.org.uk/wordpress/

A backplate design is available for laser cutting in the Laserables repository. There is also a printable design for the four standoffs needed to mount the back plate to the PCB these can be foundin the Printables repository as StanoddM3x10x18.


